# Copyright (c) Facebook, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

import os

import argparse
import csv
import logging
import pickle

import numpy as np
import torch
import transformers

import src.slurm
import src.contriever
import src.utils
import src.data
import src.normalize_text
import json
import pickle

def embed_passages(args, example, model, tokenizer):
    #profile: {id:, input:, output:, profile:[{title:, abstract:, id:},....]}
    total = 0
    input_embed, output_embed, abstract_embeds, \
          title_embeds, abstract_title_embeds = [], [], [], [], []
    

    allids, allembeddings = [], []

    batch_ids, batch_text = [], []
    batch_text = [example['input'][47:], example['output'], example['input'][47:]+ '\n' + example['output'] ]
    batch_ids.extend([example["id"]]*3)
    with torch.no_grad():
        for index, p in enumerate(example['profile']):
            batch_ids.extend([p["id"]]*3)
            title = p["title"]
            abstract = p["text"]
            if args.no_title or not "title" in p:
                text = p["text"]
            else:
                text = p["title"] + "\n" + p["text"]
            if args.lowercase:
                text, title, abstract = text.lower(), title.lower(), abstract.lower()
            if args.normalize_text:
                text, title, abstract = src.normalize_text.normalize(text),\
                    src.normalize_text.normalize(title), src.normalize_text.normalize(abstract)
            batch_text.extend([text, title, abstract])

            if len(batch_text) == args.per_gpu_batch_size or index == len(example['profile']) - 1:

                encoded_batch = tokenizer.batch_encode_plus(
                    batch_text,
                    return_tensors="pt",
                    max_length=args.passage_maxlength,
                    padding=True,
                    truncation=True,
                )

                encoded_batch = {i: v.cuda() for i, v in encoded_batch.items()}
                embeddings = model(**encoded_batch)

                embeddings = embeddings.cpu()
                total += len(batch_ids)
                allids.extend(batch_ids)
                allembeddings.append(embeddings)

                batch_text, batch_ids = [], []
                if index % 100000 == 0 and index > 0:
                    print(f"Encoded passages {total}")

    allembeddings = torch.cat(allembeddings, dim=0).numpy()
    return allids, allembeddings


def embed_all_profiles(args, passages, model, tokenizer):
    pass

def main(args):
    model, tokenizer, _ = src.contriever.load_retriever(args.model_name_or_path)
    print(f"Model loaded from {args.model_name_or_path}.", flush=True)
    model.eval()
    model = model.cuda()
    if not args.no_fp16:
        model = model.half()

    passages = src.data.load_profiles(args.profiles)
    
    shard_size = len(passages) // args.num_shards
    start_idx = args.shard_id * shard_size
    end_idx = start_idx + shard_size
    if args.shard_id == args.num_shards - 1:
        end_idx = len(passages)

    passages = passages[start_idx:end_idx]
    print(f"Embedding generation for {len(passages)} passages from idx {start_idx} to {end_idx}.")

    allids, allembeddings = embed_passages(args, passages, model, tokenizer)

    save_file = os.path.join(args.output_dir, args.prefix + f"_{args.shard_id:02d}")
    os.makedirs(args.output_dir, exist_ok=True)
    print(f"Saving {len(allids)} passage embeddings to {save_file}.")
    with open(save_file, mode="wb") as f:
        pickle.dump((allids, allembeddings), f)

    print(f"Total passages processed {len(allids)}. Written to {save_file}.")

def extract_embs(args):
    os.makedirs(args.output_dir, exist_ok=True)
    with open('./json_files/news/dev_questions.json') as f:
        dev_q = json.load(f)
    with open('./json_files/news/syn_new.json') as f:
        dev_out = json.load(f)
    model, tokenizer, _ = src.contriever.load_retriever(args.model_name_or_path)
    model.eval()
    model = model.cuda()
    for i in range(0,len(dev_q),1):
        print(i)
        example = {}
        example['id'] = dev_q[i]['id']
        example['input'] = dev_q[i]['input']
        example['output'] = dev_out['golds'][i]['output']
        example['profile'] = dev_q[i]['profile']
        allids, allembeddings = embed_passages(args, example, model, tokenizer)
        prefix = 'user' + str(dev_q[i]['id'])
        save_file = os.path.join(args.output_dir, prefix)
        with open(save_file, mode="wb") as f:
            pickle.dump((allids, allembeddings), f)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--profiles", type=str, default=None, help="Path to profiles (.json file)")
    parser.add_argument("--output_dir", type=str, default="output_all_embeddings", help="dir path to save embeddings")
    parser.add_argument("--prefix", type=str, default="passages", help="prefix path to save embeddings")
    parser.add_argument("--shard_id", type=int, default=0, help="Id of the current shard")
    parser.add_argument("--num_shards", type=int, default=1, help="Total number of shards")
    parser.add_argument(
        "--per_gpu_batch_size", type=int, default=512, help="Batch size for the passage encoder forward pass"
    )
    parser.add_argument("--passage_maxlength", type=int, default=512, help="Maximum number of tokens in a passage")
    parser.add_argument(
        "--model_name_or_path", type=str, help="path to directory containing model weights and config file"
    )
    parser.add_argument("--no_fp16", action="store_true", help="inference in fp32")
    parser.add_argument("--no_title", action="store_true", help="title not added to the passage body")
    parser.add_argument("--lowercase", action="store_true", help="lowercase text before encoding")
    parser.add_argument("--normalize_text", action="store_true", help="lowercase text before encoding")

    args = parser.parse_args()

    src.slurm.init_distributed_mode(args)

    # main(args)
    extract_embs(args)
