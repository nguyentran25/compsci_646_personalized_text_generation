import os
import argparse
import csv
import json
import logging
import pickle
import time
import glob
from pathlib import Path

import numpy as np
import torch
import transformers

import src.index
import src.contriever
import src.utils
import src.slurm
import src.data
from src.evaluation import calculate_matches
import src.normalize_text
import random

os.environ["TOKENIZERS_PARALLELISM"] = "true"

def index_search(query, ids, embeddings, n_docs=4):
    index = src.index.Indexer(768, 0, 8)
    index.index_data(ids, embeddings)
    return index.search_knn(query, n_docs)

def build_prompts(id, input_text, output_text, retrieved_ids, id_to_doc, n_shots=4):
    llm_input = {"id": id, "question": "", "answers": output_text}
    if n_shots > 0:
        prompt = "Generate titles for the following text:\n"
    else:
        prompt = "Generate title for the following text:\n"
    for i in range(n_shots):
        prompt = prompt + "Text: " + id_to_doc[retrieved_ids[i]][2] + \
            '\nTitle: ' + id_to_doc[retrieved_ids[i]][1] + '\n'
    prompt = prompt + "Text: " + input_text[47:] + '\nTitle: '
    llm_input['question'] = prompt
    return llm_input

def retrieve_profile(fn, ids_to_questions, ids_to_outputs, n_shots=4, type='base'):
    with open(fn, "rb") as fin:
        ids, embeddings = pickle.load(fin)
    input_query, output_query, input_output_query = embeddings[0], embeddings[1], embeddings[2]
    _, input_text, profile = ids_to_questions[ids[0]]
    output_text = ids_to_outputs[ids[0]][1]
    id_to_doc = {}
    for index, p in enumerate(profile):
        id_to_doc[p['id']] = [index, p['title'], p['text']]

    if n_shots == 0:
        return build_prompts(ids[0], input_text, output_text, [None], id_to_doc, n_shots=0)
    #baseline
    if type == 'base':
        retrieved_docs = index_search(np.expand_dims(input_query, axis=0), ids[3::3], embeddings[5::3], n_docs=n_shots)
        llm_input = build_prompts(ids[0], input_text, output_text, retrieved_docs[0][0], id_to_doc)
    elif type == 'type1':
        title_retrieved_docs = index_search(np.expand_dims(output_query, axis=0), ids[3::3], embeddings[5::3], n_docs=n_shots)[0][0]
        abs_retrieved_docs = index_search(np.expand_dims(input_query, axis=0), ids[3::3], embeddings[5::3], n_docs=n_shots)[0][0]
        join_docs = list(set(title_retrieved_docs).intersection(set(abs_retrieved_docs)))
        if len(join_docs) < n_shots:
          add_docs = index_search(np.expand_dims(output_query, axis=0), ids[3::3], embeddings[5::3], n_docs=n_shots-len(join_docs))
          join_docs.extend(add_docs[0][0])
        else:
          join_docs = join_docs[:n_shots]
        llm_input = build_prompts(ids[0], input_text, output_text, join_docs, id_to_doc, n_shots=n_shots)
    elif type == 'type2':
        title_retrieved_docs = index_search(np.expand_dims(output_query, axis=0), ids[3::3], embeddings[3::3], n_docs=n_shots+1)[0][0]
        abs_retrieved_docs = index_search(np.expand_dims(input_query, axis=0), ids[3::3], embeddings[4::3], n_docs=n_shots+3)[0][0]
        join_docs = list(set(title_retrieved_docs).intersection(set(abs_retrieved_docs)))
        if len(join_docs) < n_shots:
          add_docs = index_search(np.expand_dims(output_query, axis=0), ids[3::3], embeddings[5::3], n_docs=n_shots-len(join_docs))
          join_docs.extend(add_docs[0][0])
        else:
          join_docs = join_docs[:n_shots]
        llm_input = build_prompts(ids[0], input_text, output_text, join_docs, id_to_doc, n_shots=n_shots)

    return llm_input

'''
{"question":"","positive_ctxs":[{"text": ""}],"negative_ctxs":[{"text": "."}], \
 "hard_negative_ctxs":[{"text": ""}],"title":"","text":"."}
'''




def create_retrieve_json(questions, outputs, files, saved_fn, n_shots=0, type='base'):
    dict_dev_q = {}
    for index, p in enumerate(questions):
        dict_dev_q[p['id']] = [index, p['input'], p['profile']]
    dict_dev_out = {}
    for index, p in enumerate(outputs['golds']):
        dict_dev_out[p['id']] = [index, p['output']]
    llm_inputs = []
    for file in files:
        llm_inputs.append(retrieve_profile(file, dict_dev_q, dict_dev_out, n_shots=n_shots, type=type))
    with open(saved_fn, 'w') as f:
        json.dump(llm_inputs, f)
    return llm_inputs

def convert_to_evaluate_form(input_file, eval_file):
    with open(input_file) as f:
        predicteds = json.load(f)
    predicted = {'task': 'LaMP_5', 'golds': []}
    for pred, ids in zip(predicteds, dev_out['golds']):
        predicted['golds'].append({'id': ids['id'], 'output': pred['prediction']})
    with open(eval_file, 'w') as f:
        json.dump(predicted, f)

def extract_training_profile(fn, ids_to_questions, ids_to_outputs, n_shots=4, mode='train'):
    with open(fn, "rb") as fin:
        ids, embeddings = pickle.load(fin)
    input_query, output_query, _ = embeddings[0], embeddings[1], embeddings[2]
    _, input_text, profile = ids_to_questions[ids[0]]
    id_to_doc = {}
    for index, p in enumerate(profile):
        id_to_doc[p['id']] = [index, p['title'], p['text']]
    title_retrieved_docs = index_search(np.expand_dims(output_query, axis=0), ids[3::3], embeddings[3::3], n_docs=n_shots)[0][0]
    abs_retrieved_docs = index_search(np.expand_dims(input_query, axis=0), ids[3::3], embeddings[4::3], n_docs=n_shots)[0][0]
    positive_docs = list(set(title_retrieved_docs).intersection(set(abs_retrieved_docs)))
    if len(positive_docs) < 1:
          add_docs = index_search(np.expand_dims(output_query, axis=0), ids[3::3], embeddings[5::3], n_docs=2)
          positive_docs.extend(add_docs[0][0])
    if mode == 'train':
        examples = []
        negative_docs = list(set([p['id'] for p in profile])-set(positive_docs))
        if len(negative_docs) < 4:
            negative_docs = title_retrieved_docs[-4:]
            positive_docs = title_retrieved_docs[0:-4]
        for positive_id in positive_docs:
            positive_text = id_to_doc[positive_id][1] + '\n' + id_to_doc[positive_id][2]
            negative_id = random.choice(negative_docs)
            negative_text = id_to_doc[negative_id][1] + '\n' + id_to_doc[negative_id][2]
            examples.append({"question":input_text[47:],"positive_ctxs":[{"text": positive_text}],\
                    "negative_ctxs":[{"text": negative_text}]})

    else:
        negative_docs = list(set([p['id'] for p in profile])-set(positive_docs))
        negative_id = random.choice(negative_docs)
        positive_id = random.choice(positive_docs[:2])
        positive_text = id_to_doc[positive_id][1] + '\n' + id_to_doc[positive_id][2]
        negative_text = id_to_doc[negative_id][1] + '\n' + id_to_doc[negative_id][2]
        examples =  [{"question":input_text[47:],"positive_ctxs":[{"text": positive_text}],\
                    "negative_ctxs":[{"text": negative_text}]}]
    return examples

def create_training_data(questions, outputs, files, saved_fn, n_shots=4, mode='train'):
    dict_dev_q = {}
    for index, p in enumerate(questions):
        dict_dev_q[p['id']] = [index, p['input'], p['profile']]
    dict_dev_out = {}
    for index, p in enumerate(outputs['golds']):
        dict_dev_out[p['id']] = [index, p['output']]
    llm_inputs = []
    for file in files:
        llm_inputs.extend(extract_training_profile(file, dict_dev_q, dict_dev_out, n_shots=n_shots))
    with open(saved_fn, 'w') as outfile:
        for entry in llm_inputs:
            json.dump(entry, outfile)
            outfile.write('\n')
    return llm_inputs 

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--profiles_dir", type=str, default="output_all_embeddings/dev/", \
                         help="dir path to profile embeddings")
    parser.add_argument("--n_shots", type=int, default=0, help="n-shots examples")
    parser.add_argument("--type", type=str, default="base",\
                         help="type retrieval")
    parser.add_argument("--mode", type=str, default="train",\
                         help="type retrieval")
    parser.add_argument("--output_file", type=str, default="output.json",\
                         help="path to save prompt file")
    parser.add_argument("--questions_file", type=str, default='./json_files/news/dev_questions.json',\
                         help="path to save prompt file")
    parser.add_argument("--outputs_file", type=str, default='./json_files/news/dev_outputs.json',\
                         help="path to save prompt file")
    parser.add_argument("--extract_training", action="store_true", help="extract training data")
    
    args = parser.parse_args()
    files = [args.profiles_dir + f for f in os.listdir(args.profiles_dir)]
    with open(args.questions_file) as f:
        dev_q = json.load(f)
    with open(args.outputs_file) as f:
        dev_out = json.load(f)
    if args.extract_training:
        print('extract training data')
        llm_inputs = create_training_data(dev_q, dev_out, files, args.output_file, n_shots=args.n_shots, mode=args.mode)
    else:
        print('llama input')
        llm_inputs = create_retrieve_json(dev_q, dev_out, files, args.output_file, n_shots=args.n_shots, type=args.type)
